import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Random;

/**
 * Big text file generator
 */
public class Generator {
    private static Random rnd = new Random();

    public static void main(String[] args) {
        Map<String, String> options = ArgsParser.parse(args);
        File output = new File(options.getOrDefault("output", "text.txt"));
        long lines = Long.parseLong(options.getOrDefault("lines", "1000000"));
        int lineLengthMin = Integer.parseInt(options.getOrDefault("lineLengthMin", "0"));
        int lineLengthMax = Integer.parseInt(options.getOrDefault("lineLengthMax", "1000"));

        try (FileWriter writer = new FileWriter(output)) {
            for (int i = 0; i < lines; i++) {
                writer.write(generateLine(lineLengthMin + rnd.nextInt(lineLengthMax - lineLengthMin)));
                writer.write('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String generateLine(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append((char) ('a' + rnd.nextInt('z' - 'a')));
        }
        return sb.toString();
    }
}
