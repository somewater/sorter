import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Big text file sorter
 */
public class Sorter implements Runnable {
    private File input;
    private long inputStartByte;
    private long inputEndByte;
    private File output;
    private Context context;

    private static class Context {
        File tmp;
        int memoryLimit;
        AtomicInteger parallelism;
    }

    public static void main(String[] args) {
        Map<String, String> params = ArgsParser.parse(args);
        File input = new File(params.getOrDefault("input", "text.txt"));
        File output = new File(params.getOrDefault("output", input.toString() + ".sorted"));
        File tmp = new File(params.getOrDefault("tmp", "tmp"));
        int memoryLimit =
                Integer.parseInt(params.getOrDefault("memoryLimit", "1048576"));
        int maxParallelism =
                Integer.parseInt(params.getOrDefault("maxParallelism",
                        Integer.toString(Runtime.getRuntime().availableProcessors())));
        if (!input.exists()) {
            System.err.println("Input file " + input + " does not exists");
            System.exit(-1);
        }
        if (tmp.exists() && tmp.isFile()) {
            System.err.println("Tmp path " + tmp + " should be directory");
            System.exit(-1);
        } else if (!tmp.exists()) {
            tmp.mkdirs();
        }

        Context context = new Context();
        context.memoryLimit = memoryLimit;
        context.tmp = tmp;
        context.parallelism = new AtomicInteger(maxParallelism);
        Sorter sorter = new Sorter(input, 0, input.length(), output, context);
        sorter.run();
    }

    public Sorter(File input, long inputStartByte, long inputEndByte, File output, Context context) {
        this.input = input;
        this.inputStartByte = inputStartByte;
        this.inputEndByte = inputEndByte;
        this.output = output;
        this.context = context;
    }

    @Override
    public void run() {
        try {
            if (inputEndByte - inputStartByte < context.memoryLimit) {
                sort();
            } else {
                fork();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void sort() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(input));
             BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(output))) {

            reader.skip(inputStartByte);
            long pos = inputStartByte;
            List<String> lines = new ArrayList<>();
            while (pos <= inputEndByte) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                } else {
                    pos += line.getBytes().length;
                    pos++;
                    lines.add(line);
                }
            }
            lines.sort(String::compareTo);
            if (lines.isEmpty()) {
                // empty line still required trailing \n
                outputStream.write('\n');
            } else {
                for (String line : lines) {
                    outputStream.write(line.getBytes());
                    outputStream.write('\n');
                }
            }
        }
    }

    private static void merge(File input1, File input2, File output) throws IOException {
        try (BufferedReader f1 = new BufferedReader(new FileReader(input1));
             BufferedReader f2 = new BufferedReader(new FileReader(input2));
             FileWriter out = new FileWriter(output)) {
            if (f1.ready() && f2.ready()) {
                String l1 = f1.readLine();
                String l2 = f2.readLine();
                while (true) {
                    int cmp = l1.compareTo(l2);
                    if (cmp <= 0) {
                        out.write(l1);
                        out.write('\n');
                        if (f1.ready()) {
                            l1 = f1.readLine();
                        } else {
                            out.write(l2);
                            out.write('\n');
                            break;
                        }
                    } else {
                        out.write(l2);
                        out.write('\n');
                        if (f2.ready()) {
                            l2 = f2.readLine();
                        } else {
                            out.write(l1);
                            out.write('\n');
                            break;
                        }
                    }
                }
            }
            while (f1.ready()) {
                out.write(f1.readLine());
                out.write('\n');
            }
            while (f2.ready()) {
                out.write(f2.readLine());
                out.write('\n');
            }
        }
    }

    private void fork() throws IOException {
        long offset = findNewLineCharOffset();
        if (offset == -1) {
            copyFromInputToOutput();
        } else {
            File output1 = tmpFile();
            File output2 = tmpFile();
            try {
                Sorter s1 = new Sorter(input, inputStartByte, offset, output1, context);
                Sorter s2 = new Sorter(input, offset + 1, inputEndByte, output2, context);
                int value = context.parallelism.decrementAndGet();
                if (value >= 0) {
                    Thread t1 = new Thread(s1);
                    Thread t2 = new Thread(s2);
                    t1.start();
                    t2.start();
                    try {
                        t1.join();
                        t2.join();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    context.parallelism.incrementAndGet();
                } else {
                    context.parallelism.incrementAndGet();
                    s1.run();
                    s2.run();
                }
                merge(output1, output2, output);
            } finally {
                output1.delete();
                output2.delete();
            }
        }
    }

    private long findNewLineCharOffset() throws IOException {
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(input, "r")) {
            long mid = (inputEndByte - inputStartByte) / 2 + inputStartByte;
            long pos = mid;
            randomAccessFile.seek(mid);
            int b;
            while ((b = randomAccessFile.read()) != '\n') {
                pos++;
                if (b == -1 || pos == inputEndByte) {
                    // long text line, try to search in opposite direction
                    pos = mid;
                    while (--pos >= inputStartByte) {
                        randomAccessFile.seek(pos);
                        if (randomAccessFile.read() == '\n') {
                            return pos;
                        }
                    }
                    return -1;
                }
            }
            return pos;
        }
    }

    private void copyFromInputToOutput() throws IOException {
        try (FileOutputStream out = new FileOutputStream(output)) {
            Files.copy(input.toPath(), out);
        }
    }

    private File tmpFile() {
        return new File(context.tmp, UUID.randomUUID().toString());
    }
}
