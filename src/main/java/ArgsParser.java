import java.util.HashMap;
import java.util.Map;

/**
 * Parse command line args
 */
public class ArgsParser {
    public static Map<String, String> parse(String[] args) {
        Map<String, String> result = new HashMap<String, String>();
        for (int i = 0; i < args.length; i++) {
            if (!args[i].startsWith("-") && i > 0 && args[i - 1].startsWith("-")) {
                result.put(stripLeadingHyphens(args[i - 1]), args[i]);
            }
        }
        return result;
    }

    private static String stripLeadingHyphens(String value) {
        int start = 0;
        while (start < value.length())
            if (value.charAt(start) == '-') {
                start++;
            } else {
                break;
            }
        return value.substring(start);
    }
}
