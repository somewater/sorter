Run as:
```
$ mvn package

$ # limit java memory consumption for this test (or can detect it manually)
$ export JAVA_OPTS="-Xms100m -Xmx100m"

$ java -cp target/test-1.0-SNAPSHOT.jar Generator -output text.txt \
  -lines 1000000 -lineLengthMax 1000
  
$ java -cp target/test-1.0-SNAPSHOT.jar Sorter -input text.txt -output text_sorted.txt
```